$(document).ready(function() {
	if ($('.slider-main').length > 0) {
		$('.slider-main').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 1200,
			prevArrow: '<a href="#" class="prev-link"></a>',
			nextArrow: '<a href="#" class="next-link"></a>'
		});
	}
	if ($('.portfolio-main').length > 0) {
		$('.portfolio-main').slick({
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 1200,
			prevArrow: '<a href="#" class="prev-link"></a>',
			nextArrow: '<a href="#" class="next-link"></a>',
			slidesToShow: 7,
			responsive: [
			    {
			      breakpoint: 1800,
			      settings: {
			        slidesToShow: 6
			      }
			    },
			    {
			      breakpoint: 1600,
			      settings: {
			        slidesToShow: 5
			      }
			    },
			    {
			      breakpoint: 1260,
			      settings: {
			        slidesToShow: 4
			      }
			    }
		    ]
		});
		$('.portfolio-main img').each(function() {
			$(this).css('top', (125 - $(this).height())/2);
		});
	}

	$('.header-callback').magnificPopup({
		type: 'inline'
	});

	$(".contacts-map").gmap3({
	  map:{
	    options: {
	      center:[55.800519, 37.627257],
	      zoom: 16,
	      mapTypeId: google.maps.MapTypeId.TERRAIN
	    }
	  },
	  marker: {
	  	address: 'Москва, проспект Мира, 81',
	    options: {
	      icon: new google.maps.MarkerImage("/img/map-marker.png")
	    }
	  }
	});
	$('input[type=file]').nicefileinput();
	$('.NFI-filename').attr('value', 'Добавить файл');
});